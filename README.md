# 3D sketches curated dataset

This is a small dataset of 3D sketches, collected for the purpose of testing an algorithm that generates the depicted surface from an input 3D sketch [[research project page]](https://em-yu.github.io/research/surfacing_3d_sketches/).

## Format

We use the OBJ format:

```
v x y z
v ...

l 1 2 3 4 ...
l ...
```

This can be easily parsed by most 3D modelling software, such as Blender.

Caution: indices start at 1!

## Preprocessing applied

All sketches in the dataset have been:

* Centered and rescaled to fit a unit bounding box around the origin
* Converted to the OBJ format from diverse original formats, such as IGES and other custom formats, depending on the source.

Some sketches in the dataset may have been:

* Edited to be perfectly symmetric along the X axis. Sketches were rigidly transformed to have their symmetry axis be the X axis, and some were made symmetric by construction by deleting one half and mirroring. This was performed to simplify our surfacing pipeline, on sketches that were *almost* symmetric (and clearly designed to be symmetric). This can lead to slight gaps in strokes along the x=0 plane, which are not present in the original sketches.
* Resampled to a different resolution than the originals.

Broadly, do not expect the sketches in this dataset to perfectly represent the original creations. (eg: stroke order should not be expected to represent sketching order).

Original sources for most of the sketches can be found (eg in the websites listed in the sources section), so you can also refer to that.

## Sources

Below we give some information on the provenance of each sketch in the dataset. Note that there are many different types of "sketches" in the dataset, some are synthetic, others are sketched by artists with a variety of tools (VR interfaces, 2D sketching interfaces).

Tools references:

* [Gravity Sketch](https://www.gravitysketch.com/)
* [CASSIE](https://em-yu.github.io/research/cassie/): Yu, Emilie, et al. "Cassie: Curve and surface sketching in immersive environments." *Proceedings of the 2021 CHI Conference on Human Factors in Computing Systems*. 2021.
* [CurveFusion](https://lingjie0206.github.io/papers/curvefusion/): Liu, Lingjie, et al. "CurveFusion: reconstructing thin structures from RGBD sequences." *ACM Transactions on Graphics (TOG)*. 2018.
* [Flowrep](http://www.cs.ubc.ca/labs/imager/tr/2017/FlowRep/): Gori, Giorgio, et al. "Flowrep: Descriptive curve networks for free-form design shapes." *ACM Transactions on Graphics (TOG)*. 2017.
* Analytic Drawing of 3D Scaffolds: Schmidt, Ryan, et al. "Analytic drawing of 3d scaffolds." *ACM SIGGRAPH Asia 2009 papers*. 2009.
* [ILoveSketch](https://www.dgp.toronto.edu/~shbae/ilovesketch.htm): Bae, Seok-Hyung, Ravin Balakrishnan, and Karan Singh. "ILoveSketch: as-natural-as-possible sketching system for creating 3d curve models." *Proceedings of the 21st annual ACM symposium on User interface software and technology*. 2008.
* [Onshape](https://www.onshape.com/)
* [Penzil](https://www.penzil.app/): a browser-based tool for sketching in 3D, developped by Jacopo Colò.
* SketchingWithHands: Kim, Yongkwan, and Seok-Hyung Bae. "SketchingWithHands: 3D sketching handheld products with first-person hand posture." *Proceedings of the 29th Annual Symposium on User Interface Software and Technology*. 2016.
* [True2Form](http://www.cs.ubc.ca/labs/imager/tr/2014/True2Form/index.htm): Xu, Baoxuan, et al. "True2Form: 3D curve networks from 2D sketches via selective regularization." *ACM Transactions on Graphics* (2014).



| **Sketch**             | **Tool**                         | **Author**       | **Synthetic** |
| ---------------------- | -------------------------------- | ---------------- | ------------- |
| author1_building       | Gravity Sketch                   | Emilie Yu        |               |
| author1_bulbasaur      | Gravity Sketch                   | Emilie Yu        |               |
| author1_bust           | Gravity Sketch                   | Emilie Yu        |               |
| author1_car            | Gravity Sketch                   | Emilie Yu        |               |
| author1_chair          | Gravity Sketch                   | Emilie Yu        |               |
| author1_chair_ff       | Gravity Sketch                   | Emilie Yu        |               |
| author1_ship           | Gravity Sketch                   | Emilie Yu        |               |
| author2_gamepad        | Gravity Sketch                   | Rahul Arora      |               |
| author2_guitar         | Gravity Sketch                   | Rahul Arora      |               |
| author2_sofa           | Gravity Sketch                   | Rahul Arora      |               |
| cassie_car             | CASSIE                           | Priam Givord     |               |
| cassie_hat             | CASSIE                           | Emilie Yu        |               |
| cassie_vacuum          | CASSIE                           | Emilie Yu        |               |
| curvefusion_bust       | CurveFusion method               |                  | X             |
| flowrep_bathtub        | Flowrep method                   |                  | X             |
| flowrep_boat           | Flowrep method                   |                  | X             |
| flowrep_bottle         | Flowrep method                   |                  | X             |
| flowrep_ellipsetorus   | Flowrep method                   |                  | X             |
| flowrep_mouse          | Flowrep method                   |                  | X             |
| flowrep_phone          | Flowrep method                   |                  | X             |
| flowrep_spherecylinder | Flowrep method                   |                  | X             |
| flowrep_trebol         | Flowrep method                   |                  | X             |
| flowsurf_beetle        | Unknown                          |                  |               |
| flowsurf_expresso      | Analytic Drawing of 3D Scaffolds |                  |               |
| ils_boat               | ILoveSketch                      |                  |               |
| ils_car1               | ILoveSketch                      |                  |               |
| ils_car2               | ILoveSketch                      |                  |               |
| ils_car3               | ILoveSketch                      |                  |               |
| ils_car4               | ILoveSketch                      |                  |               |
| ils_car5               | ILoveSketch                      |                  |               |
| ils_roadster           | ILoveSketch                      |                  |               |
| ils_spacecraft_29      | ILoveSketch                      |                  |               |
| ils_spacecraft_78      | ILoveSketch                      |                  |               |
| ils_speaker            | ILoveSketch                      |                  |               |
| onshape_bishop         | Onshape B-rep edges              |                  | X             |
| onshape_boat           | Onshape B-rep edges              |                  | X             |
| onshape_guitar         | Onshape B-rep edges              |                  | X             |
| onshape_house          | Onshape B-rep edges              |                  | X             |
| onshape_iron           | Onshape B-rep edges              |                  | X             |
| onshape_simple_mouse   | Onshape B-rep edges              |                  | X             |
| onshape_simple_shape   | Onshape B-rep edges              |                  | X             |
| onshape_vacuum         | Onshape B-rep edges              |                  | X             |
| penzil_spaceship       | Penzil                           | Jacopo Colò      |               |
| robbins_car1           | Gravity Sketch                   | James Robbins    |               |
| robbins_car2           | Gravity Sketch                   | James Robbins    |               |
| robbins_spaceship      | Gravity Sketch                   | James Robbins    |               |
| scaffolds3d_dog        | Analytic Drawing of 3D Scaffolds |                  |               |
| scaffolds3d_submarine  | Analytic Drawing of 3D Scaffolds |                  |               |
| swh_dental_light       | SketchingWithHands               |                  |               |
| swh_vr_controller      | SketchingWithHands               |                  |               |
| t2f_blender            | True2Form                        |                  |               |
| t2f_box                | True2Form                        |                  |               |
| t2f_car_93             | True2Form                        |                  |               |
| t2f_car_97             | True2Form                        |                  |               |
| t2f_fighter            | True2Form                        |                  |               |
| t2f_guitar             | True2Form                        |                  |               |
| t2f_hand_vacuum        | True2Form                        |                  |               |
| t2f_iron               | True2Form                        |                  |               |
| t2f_sewing_machine     | True2Form                        |                  |               |
| t2f_toothpaste         | True2Form                        |                  |               |
| t2f_vacuum             | True2Form                        |                  |               |
| tolentino_shoe         | Gravity Sketch                   | Arturo Tolentino |               |



## License

Most sketches in the dataset can be freely used in an academic setting and reproduced in research papers without asking for author permission.

Exception is made of sketches by the artists: Arturo Tolentino, Jacopo Colò and James Robbins. If you are planning to reproduce those sketches in an academic paper, you have to explicitly ask permission to reproduce to the artist. Please [email me](mailto:emilie.yu@inria.fr) to obtain their contact.

